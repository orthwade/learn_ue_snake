// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_SnakeGameGameModeBase_generated_h
#error "SnakeGameGameModeBase.generated.h already included, missing '#pragma once' in SnakeGameGameModeBase.h"
#endif
#define SNAKEGAME_SnakeGameGameModeBase_generated_h

#define learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_15_SPARSE_DATA
#define learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_15_RPC_WRAPPERS
#define learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeGameGameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeGameGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeGameGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASnakeGameGameModeBase)


#define learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnakeGameGameModeBase(); \
	friend struct Z_Construct_UClass_ASnakeGameGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeGameGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASnakeGameGameModeBase)


#define learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeGameGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeGameGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeGameGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeGameGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeGameGameModeBase(ASnakeGameGameModeBase&&); \
	NO_API ASnakeGameGameModeBase(const ASnakeGameGameModeBase&); \
public:


#define learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeGameGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeGameGameModeBase(ASnakeGameGameModeBase&&); \
	NO_API ASnakeGameGameModeBase(const ASnakeGameGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeGameGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeGameGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeGameGameModeBase)


#define learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_12_PROLOG
#define learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_15_SPARSE_DATA \
	learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_15_RPC_WRAPPERS \
	learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_15_INCLASS \
	learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_15_SPARSE_DATA \
	learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class ASnakeGameGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID learn_ue_snake_Source_SnakeGame_SnakeGameGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
