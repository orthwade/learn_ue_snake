// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/KillBox.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeKillBox() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_AKillBox_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_AKillBox();
	SNAKEGAME_API UClass* Z_Construct_UClass_ADamageBox();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
// End Cross Module References
	void AKillBox::StaticRegisterNativesAKillBox()
	{
	}
	UClass* Z_Construct_UClass_AKillBox_NoRegister()
	{
		return AKillBox::StaticClass();
	}
	struct Z_Construct_UClass_AKillBox_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AKillBox_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ADamageBox,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AKillBox_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "KillBox.h" },
		{ "ModuleRelativePath", "KillBox.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AKillBox_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AKillBox>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AKillBox_Statics::ClassParams = {
		&AKillBox::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AKillBox_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AKillBox_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AKillBox()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AKillBox_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AKillBox, 1766769773);
	template<> SNAKEGAME_API UClass* StaticClass<AKillBox>()
	{
		return AKillBox::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AKillBox(Z_Construct_UClass_AKillBox, &AKillBox::StaticClass, TEXT("/Script/SnakeGame"), TEXT("AKillBox"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AKillBox);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
