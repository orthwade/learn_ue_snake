// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_DamageBox_generated_h
#error "DamageBox.generated.h already included, missing '#pragma once' in DamageBox.h"
#endif
#define SNAKEGAME_DamageBox_generated_h

#define learn_ue_snake_Source_SnakeGame_DamageBox_h_13_SPARSE_DATA
#define learn_ue_snake_Source_SnakeGame_DamageBox_h_13_RPC_WRAPPERS
#define learn_ue_snake_Source_SnakeGame_DamageBox_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define learn_ue_snake_Source_SnakeGame_DamageBox_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADamageBox(); \
	friend struct Z_Construct_UClass_ADamageBox_Statics; \
public: \
	DECLARE_CLASS(ADamageBox, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ADamageBox) \
	virtual UObject* _getUObject() const override { return const_cast<ADamageBox*>(this); }


#define learn_ue_snake_Source_SnakeGame_DamageBox_h_13_INCLASS \
private: \
	static void StaticRegisterNativesADamageBox(); \
	friend struct Z_Construct_UClass_ADamageBox_Statics; \
public: \
	DECLARE_CLASS(ADamageBox, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ADamageBox) \
	virtual UObject* _getUObject() const override { return const_cast<ADamageBox*>(this); }


#define learn_ue_snake_Source_SnakeGame_DamageBox_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADamageBox(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADamageBox) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADamageBox); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADamageBox); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADamageBox(ADamageBox&&); \
	NO_API ADamageBox(const ADamageBox&); \
public:


#define learn_ue_snake_Source_SnakeGame_DamageBox_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADamageBox(ADamageBox&&); \
	NO_API ADamageBox(const ADamageBox&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADamageBox); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADamageBox); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADamageBox)


#define learn_ue_snake_Source_SnakeGame_DamageBox_h_13_PRIVATE_PROPERTY_OFFSET
#define learn_ue_snake_Source_SnakeGame_DamageBox_h_10_PROLOG
#define learn_ue_snake_Source_SnakeGame_DamageBox_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	learn_ue_snake_Source_SnakeGame_DamageBox_h_13_PRIVATE_PROPERTY_OFFSET \
	learn_ue_snake_Source_SnakeGame_DamageBox_h_13_SPARSE_DATA \
	learn_ue_snake_Source_SnakeGame_DamageBox_h_13_RPC_WRAPPERS \
	learn_ue_snake_Source_SnakeGame_DamageBox_h_13_INCLASS \
	learn_ue_snake_Source_SnakeGame_DamageBox_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define learn_ue_snake_Source_SnakeGame_DamageBox_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	learn_ue_snake_Source_SnakeGame_DamageBox_h_13_PRIVATE_PROPERTY_OFFSET \
	learn_ue_snake_Source_SnakeGame_DamageBox_h_13_SPARSE_DATA \
	learn_ue_snake_Source_SnakeGame_DamageBox_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	learn_ue_snake_Source_SnakeGame_DamageBox_h_13_INCLASS_NO_PURE_DECLS \
	learn_ue_snake_Source_SnakeGame_DamageBox_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class ADamageBox>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID learn_ue_snake_Source_SnakeGame_DamageBox_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
