// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_KillBox_generated_h
#error "KillBox.generated.h already included, missing '#pragma once' in KillBox.h"
#endif
#define SNAKEGAME_KillBox_generated_h

#define learn_ue_snake_Source_SnakeGame_KillBox_h_15_SPARSE_DATA
#define learn_ue_snake_Source_SnakeGame_KillBox_h_15_RPC_WRAPPERS
#define learn_ue_snake_Source_SnakeGame_KillBox_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define learn_ue_snake_Source_SnakeGame_KillBox_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAKillBox(); \
	friend struct Z_Construct_UClass_AKillBox_Statics; \
public: \
	DECLARE_CLASS(AKillBox, ADamageBox, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AKillBox)


#define learn_ue_snake_Source_SnakeGame_KillBox_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAKillBox(); \
	friend struct Z_Construct_UClass_AKillBox_Statics; \
public: \
	DECLARE_CLASS(AKillBox, ADamageBox, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AKillBox)


#define learn_ue_snake_Source_SnakeGame_KillBox_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AKillBox(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AKillBox) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AKillBox); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AKillBox); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AKillBox(AKillBox&&); \
	NO_API AKillBox(const AKillBox&); \
public:


#define learn_ue_snake_Source_SnakeGame_KillBox_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AKillBox() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AKillBox(AKillBox&&); \
	NO_API AKillBox(const AKillBox&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AKillBox); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AKillBox); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AKillBox)


#define learn_ue_snake_Source_SnakeGame_KillBox_h_15_PRIVATE_PROPERTY_OFFSET
#define learn_ue_snake_Source_SnakeGame_KillBox_h_12_PROLOG
#define learn_ue_snake_Source_SnakeGame_KillBox_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	learn_ue_snake_Source_SnakeGame_KillBox_h_15_PRIVATE_PROPERTY_OFFSET \
	learn_ue_snake_Source_SnakeGame_KillBox_h_15_SPARSE_DATA \
	learn_ue_snake_Source_SnakeGame_KillBox_h_15_RPC_WRAPPERS \
	learn_ue_snake_Source_SnakeGame_KillBox_h_15_INCLASS \
	learn_ue_snake_Source_SnakeGame_KillBox_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define learn_ue_snake_Source_SnakeGame_KillBox_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	learn_ue_snake_Source_SnakeGame_KillBox_h_15_PRIVATE_PROPERTY_OFFSET \
	learn_ue_snake_Source_SnakeGame_KillBox_h_15_SPARSE_DATA \
	learn_ue_snake_Source_SnakeGame_KillBox_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	learn_ue_snake_Source_SnakeGame_KillBox_h_15_INCLASS_NO_PURE_DECLS \
	learn_ue_snake_Source_SnakeGame_KillBox_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class AKillBox>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID learn_ue_snake_Source_SnakeGame_KillBox_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
