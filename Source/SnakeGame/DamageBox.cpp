// Fill out your copyright notice in the Description page of Project Settings.


#include "DamageBox.h"
#include "SnakeBase.h"

// Sets default values
ADamageBox::ADamageBox()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADamageBox::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADamageBox::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADamageBox::Interact(AActor* _other, bool _is_head)
{
	ASnakeBase* snake = Cast<ASnakeBase>(_other);

	if (IsValid(snake))
	{
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Orange, TEXT("Snake hit hitbox. Elements decreased."));

		snake->TakeHit();

		Destroy();
	}
}

