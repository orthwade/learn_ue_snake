// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DamageBox.h"
#include "KillBox.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API AKillBox : public ADamageBox
{
	GENERATED_BODY()
public:
	virtual void Interact(AActor* _other, bool b_is_head) override;
	
};
