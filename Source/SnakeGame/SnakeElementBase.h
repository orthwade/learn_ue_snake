// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeElementBase.generated.h"

class UStaticMeshComponent;
class ASnakeBase;

UCLASS()
class SNAKEGAME_API ASnakeElementBase : public AActor , public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeElementBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

protected:
	FVector m_pos_0;
	FVector m_pos_1;
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY()
	ASnakeBase* SnakeOwner;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	inline void Move(const FVector& DeltaMovement) { AddActorWorldOffset(DeltaMovement); }
	
	inline void SetPos(const FVector& _pos) { SetActorLocation(_pos); }

	inline void SetPos0(const FVector& _pos_0) { m_pos_0 = _pos_0; }
	inline void SetPos1(const FVector& _pos_1) { m_pos_1 = _pos_1; }

	inline FVector GetPos() const { return GetActorTransform().GetLocation(); }

	inline const FVector& GetPos0() const { return m_pos_0; }
	inline const FVector& GetPos1() const { return m_pos_1; }

	inline void Move(float DeltaTime, float _speed) { AddActorWorldOffset((m_pos_1 - m_pos_0).GetSafeNormal() * DeltaTime * _speed); }

	void SetFirstElementType();

	inline void SetSnakeOwner(ASnakeBase* _owner) { SnakeOwner = _owner; }

	virtual void Interact(AActor* _actor, bool bIsHead) override;

	UFUNCTION()
	void HandleBeginOverlap(
		UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherActorComponent,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);

	UFUNCTION()
	void ToggleCollision();
};
