// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
	:
	ElemStep(100.0f),
	MovementSpeed(100.0f),
	CurrentMovementDirection(EMovementDirection::UP),
	NextMovementDirection(EMovementDirection::UP)

{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

FVector ASnakeBase::GetNextShift(const EMovementDirection& _direction)
{
	FVector result{ 0.0f, 0.0f, 0.0f };

	switch (_direction)
	{
	case EMovementDirection::UP:
		result.X += m_cell_dim;
		break;
	case EMovementDirection::DOWN:
		result.X -= m_cell_dim;
		break;
	case EMovementDirection::LEFT:
		result.Y -= m_cell_dim;
		break;
	case EMovementDirection::RIGHT:
		result.Y += m_cell_dim;
		break;
	default:
		break;
	}

	return result;
}

EMovementDirection ASnakeBase::GetReverseMovementDirection(EMovementDirection _direction)
{
	EMovementDirection result;

	switch (_direction)
	{
	case EMovementDirection::RIGHT :
		result = EMovementDirection::LEFT;
		break;
	case EMovementDirection::LEFT :
		result = EMovementDirection::RIGHT;
		break;
	case EMovementDirection::UP :
		result = EMovementDirection::DOWN;
		break;
	case EMovementDirection::DOWN:
		result = EMovementDirection::UP;
		break;
	default:
		break;
	}

	return result;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	CurrentMovementDirection = NextMovementDirection = EMovementDirection::UP;
	AddSnakeElement(5);
	m_move_timer = 0.0f;
}

void ASnakeBase::SetCurrentMovementDirection(const EMovementDirection& _direction)
{
	CurrentMovementDirection = _direction;

	FVector location = SnakeElements[0]->GetPos();

	location += GetNextShift(CurrentMovementDirection);

	SnakeElements[0]->SetPos1(location);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (m_movement_type == EMovement::DISCRETE)
	{
		m_move_timer += DeltaTime;

		if (m_move_timer >= m_move_time_step)
		{
			Move();
			m_move_timer = 0.0f;
		}
	}
	else
		Move(DeltaTime);

	

}

void ASnakeBase::AddSnakeElement(size_t ElementsCount)
{
	for (size_t i = 0; i < ElementsCount; ++i)
	{
		FTransform NewTransform{ SnakeElements.Num() == 0 ? GetActorLocation() : 
			SnakeElements.Last()->GetPos0() + GetNextShift(GetReverseMovementDirection(CurrentMovementDirection))};

		ASnakeElementBase* NewElement{ GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform) };

		// NewElement->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);

		NewElement->SetPos0(NewTransform.GetLocation());

		if (SnakeElements.Num() != 0)
			NewElement->SetPos1(SnakeElements.Last()->GetPos0());
		else
		{
			FVector location = NewTransform.GetLocation();

			location += GetNextShift(CurrentMovementDirection);

			NewElement->SetPos1(location);

			NewElement->SetFirstElementType();

		}

		NewElement->SetSnakeOwner(this);

		SnakeElements.Add(NewElement);
	}
}

void ASnakeBase::Move()
{
	ASnakeElementBase& elem_0 = *SnakeElements[0];
	
	elem_0.ToggleCollision();

	FVector elem_0_pos_0 = elem_0.GetPos1();
	FVector elem_0_pos_1 = elem_0_pos_0 + GetNextShift(NextMovementDirection);

	elem_0.SetPos(elem_0_pos_0);
	elem_0.SetPos0(elem_0_pos_0);
	elem_0.SetPos1(elem_0_pos_1);

	for (size_t i = 1; i < SnakeElements.Num(); ++i)
	{
		ASnakeElementBase& elem = *SnakeElements[i];
		ASnakeElementBase& elem_prev = *SnakeElements[i - 1];

		elem.ToggleCollision();

		FVector elem_prev_pos_0 = elem_prev.GetPos0();
		FVector elem_pos_0 = elem.GetPos1();
		FVector elem_pos_1 = elem_prev_pos_0;

		elem.SetPos(elem_pos_0);
		elem.SetPos0(elem_pos_0);
		elem.SetPos1(elem_pos_1);

		elem.ToggleCollision();
	}

	elem_0.ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* SnakeElement, AActor* Other)
{
	if (IsValid(SnakeElement))
	{
		bool is_first_element = false;

		int32 element_index = SnakeElements.Find(SnakeElement);

		is_first_element = element_index == 0;

		IInteractable* interactable = Cast<IInteractable>(Other);

		if (interactable)
		{
			interactable->Interact(this, is_first_element);
		}
	}
}

void ASnakeBase::Kill()
{
	for (size_t i = 0; i < SnakeElements.Num(); ++i)
		SnakeElements[i]->Destroy();

	Destroy();
}

void ASnakeBase::TakeHit(size_t _hit_points)
{
	if (SnakeElements.Num() == 0)
		return;
	
	if (_hit_points >= SnakeElements.Num())
	{
		GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Orange, TEXT("Elements count zero. End game."));

		Kill();
	}
	else
	{
		for (size_t i = 0; i < _hit_points; ++i)
		{
			size_t i_ = SnakeElements.Num() - i - 1;

			SnakeElements[i_]->Destroy();

			SnakeElements.Pop();
		}
	}

}

void ASnakeBase::Move(float DeltaTime)
{
	this->SnakeElements[0]->Move(DeltaTime, MovementSpeed);

	FVector pos = SnakeElements[0]->GetPos();

	const FVector &pos_0 = SnakeElements[0]->GetPos0();
	const FVector &pos_1 = SnakeElements[0]->GetPos1();

	bool reached_desination = false;



	switch (CurrentMovementDirection)
	{
	case EMovementDirection::UP:
		reached_desination = pos.X >= pos_1.X;
		break;
	case EMovementDirection::DOWN:
		reached_desination = pos.X <= pos_1.X;
		break;
	case EMovementDirection::LEFT:
		reached_desination = pos.Y <= pos_1.Y;
		break;
	case EMovementDirection::RIGHT:
		reached_desination = pos.Y >= pos_1.Y;
		break;
	default:
		break;
	}

	if (reached_desination)
	{
		ASnakeElementBase& elem_0 = *SnakeElements[0];

		FVector elem_0_pos_0 = elem_0.GetPos1();
		FVector elem_0_pos_1 = elem_0_pos_0 + GetNextShift(NextMovementDirection);

		elem_0.SetPos(elem_0_pos_0);
		elem_0.SetPos0(elem_0_pos_0);
		elem_0.SetPos1(elem_0_pos_1);

		CurrentMovementDirection = NextMovementDirection;

		for (size_t i = 1; i < SnakeElements.Num(); ++i)
		{
			ASnakeElementBase& elem = *SnakeElements[i];
			ASnakeElementBase& elem_prev = *SnakeElements[i - 1];

			FVector elem_prev_pos_0 = elem_prev.GetPos0();
			FVector elem_pos_0 = elem.GetPos1();
			FVector elem_pos_1 = elem_prev_pos_0;

			elem.SetPos(elem_pos_0);
			elem.SetPos0(elem_pos_0);
			elem.SetPos1(elem_pos_1);
		}
	}
	else
	{
		for (size_t i = 1; i < SnakeElements.Num(); ++i)
			SnakeElements[i]->Move(DeltaTime, MovementSpeed);
	}



	// AddActorWorldOffset(MovementVector);
}